<!--
SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>

SPDX-License-Identifier: GPL-3.0-only
-->

# VT Service sequence

```plantuml
@startuml
participant "InfluxDB" as I
participant "gRPC client" as G
participant ChamberService as V
participant "OPC UA Server (PLC)" as O

== START SERVICE ==
note over V
Load configuration file "ChamberService.yaml"
- Read OPC variables
- Read Influx DB Name + credentials

end note

note over V: Start gRPC service
note over V: Start ChamberService

V->I: Create database

V->O: Read temperature X
V<-O: Temperature X is Y
V->I: Store temperature
...

V->O: Read Pressure X
V<-O: Pressure X is Y
V->I: Store Pressure
...
G->V: StartVenting_CMD
note over V: See StateChart
G<-V: OK/NOK
...
G->V: StopVenting_CMD
note over V: See StateChart
G<-V: OK/NOK
...
G->V: StartPump_CMD
note over V: See StateChart
G<-V: OK/NOK
...
G->V: StopPump_CMD
note over V: See StateChart
G<-V: OK/NOK
...

@enduml
```

```plantuml
@startuml
(*) --> WaitFor_gRPC
WaitFor_gRPC --> ===B1=== 
===B1=== --> "StartVenting_CMD"
  if "TurboFreq > Limit || Pumps_Enabled" then
    --> [NOT_ALLOWED] SendRPCResponse
  else
    --> [StartVenting] SendOPCMsg
  endif
===B1=== --> "StartPump_CMD"
  if "HighVacuumPump && Pressure > Limit" then
    --> [NOT_ALLOWED] SendRPCResponse
  else
    --> [StartHighVacuumPump] SendOPCMsg
  endif
===B1=== --> "StopPump_CMD"
  if "LowVacuumPump && TurboPump_ENABLED" then
    --> [NOT_ALLOWED] SendRPCResponse
  else
    --> [StopLowVacuumPump] SendOPCMsg
  endif
===B1=== --> "StopVenting_CMD"
    --> [StopVenting] SendOPCMsg
  SendOPCMsg --> [OK] SendRPCResponse
  SendRPCResponse --> WaitFor_gRPC
@enduml
```