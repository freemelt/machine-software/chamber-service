# SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: GPL-3.0-only

# Built-in
import logging

# PyPI
import grpc

# Freemelt
from servicelib.mqtt import MQTTMessage

# Project
from .freemeltapi import ChamberServiceRpc_pb2, ChamberServiceRpc_pb2_grpc
from .metricspub import MetricsPublisher

LOG = logging.getLogger(__name__)


class Servicer(ChamberServiceRpc_pb2_grpc.ChamberServiceServicer):

    valid_pumps = ["gun_turbo", "powder_turbo", "build_turbo"]
    valid_valves = ["gun_valve", "powder_valve", "build_valve"]

    def __init__(self, cfg, opc_client, mqtt_client):
        super().__init__()
        self.cfg = cfg
        self.opc = opc_client
        self.mqtt = mqtt_client

        # Create threads
        topic_base = self.cfg["Service:MQTT:TopicBase"]
        self.metrics_pub = MetricsPublisher(
            opc_client,
            mqtt_client,
            topic_base,
            interval=1,
            name="ThreadPub",
            daemon=True,
        )

    def __enter__(self):
        # Start threads
        self.metrics_pub.start()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        LOG.info("Stopping service threads ...")
        self.metrics_pub.stop()
        self.metrics_pub.join()
        LOG.info("Stopped service threads.")

    def pub_pump_state(self, pump, state):
        topic_base = self.cfg["Service:MQTT:TopicBase"]
        topic = f"{topic_base}/Backingpumps/Position/{pump}"
        m = MQTTMessage(topic, {"On": state})
        self.mqtt.publish(m.topic, m.payload, retain=True)

    def pub_valve_state(self, valve, state):
        topic_base = self.cfg["Service:MQTT:TopicBase"]
        topic = f"{topic_base}/Ventilation/Position/{valve}"
        m = MQTTMessage(topic, {"Open": state})
        self.mqtt.publish(m.topic, m.payload, retain=True)

    def LowVacuumGenerationControl(self, request, context):
        LOG.info("Low Vacuum generation control request:\n%r", request)

        # Start or stop backing pump
        enable = request.active

        # Get all valve values
        valves_open = dict(
            eGun=self.opc.get_bool("Ventilation:GunEnable"),
            Build=self.opc.get_bool("Ventilation:BuildEnable"),
            Powder=self.opc.get_bool("Ventilation:PowderEnable"),
        )

        # The backing pump is not allowed to start if at least one
        # value is open.
        for valve_name, valve_is_open in valves_open.items():
            if enable and valve_is_open:
                err_msg = (
                    f"Can't start Backing Pump because "
                    f"{valve_name} Turbo Vent is open."
                )
                LOG.warning(err_msg)
                context.set_details(err_msg)
                context.set_code(grpc.StatusCode.FAILED_PRECONDITION)
                return ChamberServiceRpc_pb2.CommandStatus()

        # Get all pump values
        pumps_on = dict(
            eGun=self.opc.get_bool("TurboPump:GunEnable"),
            Build=self.opc.get_bool("TurboPump:BuildEnable"),
            Powder=self.opc.get_bool("TurboPump:PowderEnable"),
        )

        # The backing pump is not allowed to stop if at least one turbo
        # pump is on.
        for pump_name, pump_is_on in pumps_on.items():
            if not enable and pump_is_on:
                err_msg = (
                    f"Can't stop Backing Pump because "
                    f"{pump_name} Turbo Pump is enabled."
                )
                LOG.warning(err_msg)
                context.set_details(err_msg)
                context.set_code(grpc.StatusCode.FAILED_PRECONDITION)
                return ChamberServiceRpc_pb2.CommandStatus()

        # Enable Backing Pump, regardless if they are already active?
        self.opc.set_bool("BackingPump:Enable", enable)
        self.pub_pump_state("Back", enable)
        return ChamberServiceRpc_pb2.CommandStatus()

    def HighVacuumGenerationControl(self, request, context):
        LOG.info("High Vacuum generation control request:\n%r", request)

        # Start or stop pumps
        enable = request.active

        # Don't start if the Backing Pump is not enabled
        if enable and not self.opc.get_bool("BackingPump:Enable"):
            err_msg = (
                "TurboPump is not allowed to start because "
                "Backing Pump is not enabled."
            )
            LOG.warning(err_msg)
            context.set_details(err_msg)
            context.set_code(grpc.StatusCode.FAILED_PRECONDITION)
            return ChamberServiceRpc_pb2.CommandStatus()

        # Don't start if the pressure is above a certain level
        max_pressure = self.cfg.get_float("Criteria:HighVacuumPumpStart_PressureMax")
        gp = self.opc.get_float("Pressure:Gun")
        bp = self.opc.get_float("Pressure:Build")
        pp = self.opc.get_float("Pressure:Powder")
        LOG.info(
            "Gun Pressure: %.3f mBar, Build Pressure: %.3f mBar, Powder "
            "Pressure: %.3f mBar. Max allowed pressure: %.3f mBar.",
            gp,
            bp,
            pp,
            max_pressure,
        )
        if enable and max(gp, bp, pp) > max_pressure:
            err_msg = (
                f"Pressure is above max allowed ({max_pressure:.3f} mBar). "
                f"Pumps are not allowed to start"
            )
            LOG.warning(err_msg)
            context.set_details(err_msg)
            context.set_code(grpc.StatusCode.FAILED_PRECONDITION)
            return ChamberServiceRpc_pb2.CommandStatus()

        # Empty generator names means to use all
        pumps = request.generator_name
        if not pumps:
            pumps = self.valid_pumps

        # Validate input
        if not set(pumps) <= set(self.valid_pumps):
            err_msg = f"Unknown pump(s) {pumps!r}."
            LOG.error(err_msg)
            context.set_details(err_msg)
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            return ChamberServiceRpc_pb2.CommandStatus()

        # Change pumps based on request
        if "gun_turbo" in pumps:
            self.opc.set_bool("TurboPump:GunEnable", enable)
            self.pub_pump_state("eGun", enable)
        if "build_turbo" in pumps:
            self.opc.set_bool("TurboPump:BuildEnable", enable)
            self.pub_pump_state("Build", enable)
        if "powder_turbo" in pumps:
            self.opc.set_bool("TurboPump:PowderEnable", enable)
            self.pub_pump_state("Powder", enable)
        return ChamberServiceRpc_pb2.CommandStatus()

    def VentingControl(self, request, context):
        LOG.info("Venting generation control request:\n%r", request)

        # Start or stop ventilation
        enable = request.ventilate

        # Make sure no pumps are enabled
        pumps_on = dict(
            eGun=self.opc.get_bool("TurboPump:GunEnable"),
            Build=self.opc.get_bool("TurboPump:BuildEnable"),
            Powder=self.opc.get_bool("TurboPump:PowderEnable"),
            Back=self.opc.get_bool("BackingPump:Enable"),
        )
        for pump_name, pump_is_on in pumps_on.items():
            if enable and pump_is_on:
                err_msg = (
                    f"Can't open valve because " f"{pump_name} Turbo Pump is enabled."
                )
                LOG.warning(err_msg)
                context.set_details(err_msg)
                context.set_code(grpc.StatusCode.FAILED_PRECONDITION)
                return ChamberServiceRpc_pb2.CommandStatus()

        # Make sure pump frequencies are not too high
        max_freq = self.cfg.get_int("Criteria:VentingStart_PumpFreqMax")
        gf = self.opc.get_float("TurboPump:GunFreq")
        bf = self.opc.get_float("TurboPump:BuildFreq")
        pf = self.opc.get_float("TurboPump:PowderFreq")
        LOG.info(
            "Gun Pump freq: %.3f Hz, Build Pump freq: %.3f Hz, Powder "
            "Pump freq: %.3f Hz. Max allowed frequency: %.3f Hz.",
            gf,
            bf,
            pf,
            max_freq,
        )
        if enable and max(gf, bf, pf) > max_freq:
            err_msg = (
                f"Pump frequency is above max allowed ({max_freq:.3f} Hz). "
                f"Can't open valve."
            )
            LOG.warning(err_msg)
            context.set_details(err_msg)
            context.set_code(grpc.StatusCode.FAILED_PRECONDITION)
            return ChamberServiceRpc_pb2.CommandStatus()

        # Empty source means to use all
        valves = request.source
        if not valves:
            valves = self.valid_valves

        # Validate input, source is not empty
        if not set(valves) <= set(self.valid_valves):
            err_msg = f"Unknown valve(s) {valves!r}"
            LOG.error(err_msg)
            context.set_details(err_msg)
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            return ChamberServiceRpc_pb2.CommandStatus()

        # Change valves based on request
        if "gun_valve" in valves:
            self.opc.set_bool("Ventilation:GunEnable", enable)
            self.pub_valve_state("eGun", enable)
        if "build_valve" in valves:
            self.opc.set_bool("Ventilation:BuildEnable", enable)
            self.pub_valve_state("Build", enable)
        if "powder_valve" in valves:
            self.opc.set_bool("Ventilation:PowderEnable", enable)
            self.pub_valve_state("Powder", enable)
        return ChamberServiceRpc_pb2.CommandStatus()

    def VacuumLampControl(self, request, context):
        LOG.info("Vacuum Lamp request:\n%r", request)
        self.opc.set_bool("VacuumLamp", request.on)
        return ChamberServiceRpc_pb2.CommandStatus()

    def CoilSectionFanControl(self, request, context):
        LOG.info("Coil section fan request:\n%r", request)
        self.opc.set_bool("CoilSectionFan", request.on)
        return ChamberServiceRpc_pb2.CommandStatus()
