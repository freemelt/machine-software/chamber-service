#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: GPL-3.0-only

"""This module is the entry point of the ChamberService"""

# Built-in
import argparse
import contextlib
import logging
import time
import sys

# Freemelt
from servicelib import Configurator, confighelpers

# Project
from chamberservice.grpc_servicer import Servicer
from chamberservice.freemeltapi import ChamberServiceRpc_pb2_grpc as rpc

LOG = logging.getLogger(__name__)


def main(args):
    """Service main function"""
    LOG.info("ChamberService started.")

    # Load service configuration
    cfg = Configurator.load_config("chamberservice.yaml")
    confighelpers.setup_logging(cfg, journal_kwargs=dict(SYSLOG_IDENTIFIER=__package__))

    # Service enter/exit stack
    with contextlib.ExitStack() as stack:
        mqtt_client = stack.enter_context(confighelpers.get_mqtt_client(cfg))
        opc_client = stack.enter_context(confighelpers.get_opc_client(cfg))
        servicer = stack.enter_context(Servicer(cfg, opc_client, mqtt_client))
        server = stack.enter_context(confighelpers.get_grpc_server(cfg, rpc, servicer))

        try:
            while True:
                opc_client.get_value("Pressure:Build", log=False)
                time.sleep(1)
        except KeyboardInterrupt:
            LOG.info("Shutdown event triggered!")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--client", action="store_true", help="Start minimal TK GUI gRPC client"
    )
    args = parser.parse_args()
    if args.client:
        # Start minimal TK GUI gRPC client
        from chamberservice import client

        client.main(args)
    else:
        try:
            main(args)
        except Exception:
            # This is added to make sure journal receives the traceback
            LOG.exception("Service failure")
            sys.exit(1)
