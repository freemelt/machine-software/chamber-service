# SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: GPL-3.0-only

"""Continuously read sensor data and publish to mqtt broker"""

# Freemelt
from servicelib.mqtt import MQTTMessage, BaseMetricsPublisherThread


class MetricsPublisher(BaseMetricsPublisherThread):
    """Continuously publish metrics to mqtt broker."""

    def __init__(self, opc_client, mqtt_client, topic_base, **kwargs):
        super().__init__(mqtt_client, **kwargs)
        self.opc = opc_client
        self.topic_base = topic_base

    def get_messages(self):
        messages = list()
        messages += self.read_device("Gun")
        messages += self.read_device("Build")
        messages += self.read_device("Powder")
        messages += self.read_pressure()
        messages += self.read_temperature()
        messages += self.read_pump_status()
        messages += self.read_valve_status()
        messages += self.read_misc()
        return messages

    def read_device(self, device):
        # Device must match variable prefix in the config file
        self.log.debug("Reading OPC variables of Turbopump %s", device)
        curr = self.opc.get_int(f"TurboPump:{device}Current")
        freq = self.opc.get_int(f"TurboPump:{device}Freq")
        temp = self.opc.get_int(f"TurboPump:{device}Temp")

        topic = f"{self.topic_base}/Turbopumps/Position/{device}"
        m1 = MQTTMessage(
            topic,
            {
                "Current": curr * 0.1,  # Current measured in 100 mA steps
                "Frequency": freq,
                "Temperature": temp,
            },
        )
        return [m1]

    def read_pressure(self):
        self.log.debug("Reading OPC variables for pressures.")
        p1 = self.opc.get_float("Pressure:Gun")
        p2 = self.opc.get_float("Pressure:Build")
        p3 = self.opc.get_float("Pressure:Powder")
        p4 = self.opc.get_float("Pressure:Backing")

        topic = f"{self.topic_base}/Vacuum/Position"
        m1 = MQTTMessage(f"{topic}/eGun", {"Pressure": p1})
        m2 = MQTTMessage(f"{topic}/Build", {"Pressure": p2})
        m3 = MQTTMessage(f"{topic}/Powder", {"Pressure": p3})
        m4 = MQTTMessage(f"{topic}/Back", {"Pressure": p4})
        return [m1, m2, m3, m4]

    def read_temperature(self):
        self.log.debug("Reading OPC variables for temperatures.")
        t1 = self.opc.get_float("Temperature:Sensor1")
        t2 = self.opc.get_float("Temperature:Sensor2")
        t3 = self.opc.get_float("Temperature:Sensor3")
        t4 = self.opc.get_float("Temperature:Sensor4")

        # The value of 10G Celcius is returned when a sensor is not
        # available. Set to zero instead.
        t1 = int(t1 < 10_000_000_000) * t1
        t2 = int(t2 < 10_000_000_000) * t2
        t3 = int(t3 < 10_000_000_000) * t3
        t4 = int(t4 < 10_000_000_000) * t4

        topic = f"{self.topic_base}/BuildTemperature/Name"
        m1 = MQTTMessage(f"{topic}/Sensor1", {"Temperature": t1})
        m2 = MQTTMessage(f"{topic}/Sensor2", {"Temperature": t2})
        m3 = MQTTMessage(f"{topic}/Sensor3", {"Temperature": t3})
        m4 = MQTTMessage(f"{topic}/Sensor4", {"Temperature": t4})
        return [m1, m2, m3, m4]

    def read_pump_status(self):
        self.log.debug("Reading OPC variables for pumps (on/off).")
        b1 = self.opc.get_bool("TurboPump:GunEnable")
        b2 = self.opc.get_bool("TurboPump:BuildEnable")
        b3 = self.opc.get_bool("TurboPump:PowderEnable")
        b4 = self.opc.get_bool("BackingPump:Enable")

        # Note: These topics are also published in gRPC service methods
        #       right before and after turning pumps on/off.
        topic = f"{self.topic_base}/Backingpumps/Position"
        m1 = MQTTMessage(f"{topic}/eGun", {"On": b1})
        m2 = MQTTMessage(f"{topic}/Build", {"On": b2})
        m3 = MQTTMessage(f"{topic}/Powder", {"On": b3})
        m4 = MQTTMessage(f"{topic}/Back", {"On": b4})
        return [m1, m2, m3, m4]

    def read_valve_status(self):
        self.log.debug("Reading OPC variables for valves (open/closed)")
        b1 = self.opc.get_bool("Ventilation:GunEnable")
        b2 = self.opc.get_bool("Ventilation:BuildEnable")
        b3 = self.opc.get_bool("Ventilation:PowderEnable")

        # Note: These topics are also published in gRPC service methods
        #       right before and after opening/closing valves.
        topic = f"{self.topic_base}/Ventilation/Position"
        m1 = MQTTMessage(f"{topic}/eGun", {"Open": b1})
        m2 = MQTTMessage(f"{topic}/Build", {"Open": b2})
        m3 = MQTTMessage(f"{topic}/Powder", {"Open": b3})
        return [m1, m2, m3]

    def read_misc(self):
        b1 = self.opc.get_bool("VacuumLamp")

        topic = f"{self.topic_base}/Light/VacuumLamp/0"
        m1 = MQTTMessage(topic, {"On": b1})
        return [m1]
