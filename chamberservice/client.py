# SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: GPL-3.0-only

# Built-in
from tkinter import *
from tkinter import ttk
import json

# PyPI
import grpc
import paho.mqtt.client as mqtt

# Freemelt
from servicelib import Configurator

# Project
from .freemeltapi import ChamberServiceRpc_pb2 as msg, ChamberServiceRpc_pb2_grpc as rpc


class MqttFrame(ttk.LabelFrame):
    def __init__(self, master, mqtt_client, topic_base, *args, **kwargs):
        title = self.__class__.__name__.replace("Frame", "")
        super().__init__(master, *args, text=title, **kwargs)
        self.client = mqtt_client
        self.topic_base = topic_base
        self.client.subscribe(f"{topic_base}/#")
        self.client.on_message = self.on_message

        self.service_alive = StringVar(value="Service alive: N/A")

        self.on_back = StringVar(value="Back pump on: N/A")
        self.on_gun = StringVar(value="eGun pump on: N/A")
        self.on_build = StringVar(value="Build pump on: N/A")
        self.on_powder = StringVar(value="Powder pump on: N/A")

        self.open_gun = StringVar(value="eGun valve open: N/A")
        self.open_build = StringVar(value="Build valve open: N/A")
        self.open_powder = StringVar(value="Powder valve open: N/A")

        self.pressure_back = StringVar(value="Back pressure: N/A")
        self.pressure_gun = StringVar(value="eGun pressure: N/A")
        self.pressure_build = StringVar(value="Build pressure: N/A")
        self.pressure_powder = StringVar(value="Powder pressure: N/A")

        self.freq_gun = StringVar(value="eGun freq: N/A")
        self.freq_build = StringVar(value="Build freq: N/A")
        self.freq_powder = StringVar(value="Powder freq: N/A")

        self.temp_1 = StringVar(value="Sensor1 temperature: N/A")
        self.temp_2 = StringVar(value="Sensor2 temperature: N/A")
        self.temp_3 = StringVar(value="Sensor3 temperature: N/A")
        self.temp_4 = StringVar(value="Sensor4 temperature: N/A")

        self.create_widgets()
        self.create_grid()

    def create_widgets(self):
        self.lb_service_alive = Label(self, textvariable=self.service_alive)

        self.lb_on_back = Label(self, textvariable=self.on_back)
        self.lb_on_gun = Label(self, textvariable=self.on_gun)
        self.lb_on_build = Label(self, textvariable=self.on_build)
        self.lb_on_powder = Label(self, textvariable=self.on_powder)

        self.lb_open_gun = Label(self, textvariable=self.open_gun)
        self.lb_open_build = Label(self, textvariable=self.open_build)
        self.lb_open_powder = Label(self, textvariable=self.open_powder)

        self.lb_pressure_back = Label(self, textvariable=self.pressure_back)
        self.lb_pressure_gun = Label(self, textvariable=self.pressure_gun)
        self.lb_pressure_build = Label(self, textvariable=self.pressure_build)
        self.lb_pressure_powder = Label(self, textvariable=self.pressure_powder)

        self.lb_freq_gun = Label(self, textvariable=self.freq_gun)
        self.lb_freq_build = Label(self, textvariable=self.freq_build)
        self.lb_freq_powder = Label(self, textvariable=self.freq_powder)

        self.lb_temp_1 = Label(self, textvariable=self.temp_1)
        self.lb_temp_2 = Label(self, textvariable=self.temp_2)
        self.lb_temp_3 = Label(self, textvariable=self.temp_3)
        self.lb_temp_4 = Label(self, textvariable=self.temp_4)

    def create_grid(self):
        self.lb_service_alive.grid(column=0, row=0, sticky="W")

        self.lb_on_back.grid(column=0, row=1, sticky="W", pady=(10, 0))
        self.lb_on_gun.grid(column=0, row=2, sticky="W")
        self.lb_on_build.grid(column=0, row=3, sticky="W")
        self.lb_on_powder.grid(column=0, row=4, sticky="W")

        self.lb_open_gun.grid(column=0, row=5, sticky="W", pady=(10, 0))
        self.lb_open_build.grid(column=0, row=6, sticky="W")
        self.lb_open_powder.grid(column=0, row=7, sticky="W")

        self.lb_pressure_back.grid(column=0, row=8, sticky="W", pady=(10, 0))
        self.lb_pressure_gun.grid(column=0, row=9, sticky="W")
        self.lb_pressure_build.grid(column=0, row=10, sticky="W")
        self.lb_pressure_powder.grid(column=0, row=11, sticky="W")

        self.lb_freq_gun.grid(column=0, row=13, sticky="W", pady=(10, 0))
        self.lb_freq_build.grid(column=0, row=14, sticky="W")
        self.lb_freq_powder.grid(column=0, row=15, sticky="W")

        self.lb_temp_1.grid(column=0, row=16, sticky="W", pady=(10, 0))
        self.lb_temp_2.grid(column=0, row=17, sticky="W")
        self.lb_temp_3.grid(column=0, row=18, sticky="W")
        self.lb_temp_4.grid(column=0, row=19, sticky="W")

    def on_message(self, client, userdata, message):
        data = json.loads(message.payload.decode("utf-8"))
        if message.topic == f"{self.topic_base}/ComponentStatus/Status/Current":
            self.service_alive.set(f'Service alive: {data["alive"]}')

        elif message.topic == f"{self.topic_base}/Backingpumps/Position/Back":
            self.on_back.set(f'Back pump on: {data["On"]}')
        elif message.topic == f"{self.topic_base}/Backingpumps/Position/eGun":
            self.on_gun.set(f'eGun pump on: {data["On"]}')
        elif message.topic == f"{self.topic_base}/Backingpumps/Position/Build":
            self.on_build.set(f'Build pump on: {data["On"]}')
        elif message.topic == f"{self.topic_base}/Backingpumps/Position/Powder":
            self.on_powder.set(f'Powder pump on: {data["On"]}')

        elif message.topic == f"{self.topic_base}/Ventilation/Position/eGun":
            self.open_gun.set(f'eGun valve open: {data["Open"]}')
        elif message.topic == f"{self.topic_base}/Ventilation/Position/Build":
            self.open_build.set(f'Build valve open: {data["Open"]}')
        elif message.topic == f"{self.topic_base}/Ventilation/Position/Powder":
            self.open_powder.set(f'Powder valve open: {data["Open"]}')

        elif message.topic == f"{self.topic_base}/Vacuum/Position/Back":
            self.pressure_back.set(f'Back pressure: {data["Pressure"]:.2e}')
        elif message.topic == f"{self.topic_base}/Vacuum/Position/eGun":
            self.pressure_gun.set(f'eGun pressure: {data["Pressure"]:.2e}')
        elif message.topic == f"{self.topic_base}/Vacuum/Position/Build":
            self.pressure_build.set(f'Build pressure: {data["Pressure"]:.2e}')
        elif message.topic == f"{self.topic_base}/Vacuum/Position/Powder":
            self.pressure_powder.set(f'Powder pressure: {data["Pressure"]:.2e}')

        elif message.topic == f"{self.topic_base}/Turbopumps/Position/Gun":
            self.freq_gun.set(f'eGun freq: {data["Frequency"]:.2f}')
        elif message.topic == f"{self.topic_base}/Turbopumps/Position/Build":
            self.freq_build.set(f'Build freq: {data["Frequency"]:.2f}')
        elif message.topic == f"{self.topic_base}/Turbopumps/Position/Powder":
            self.freq_powder.set(f'Powder freq: {data["Frequency"]:.2f}')

        elif message.topic == f"{self.topic_base}/BuildTemperature/Name/Sensor1":
            self.temp_1.set(f'Sensor1 temperature: {data["Temperature"]:.2f}')
        elif message.topic == f"{self.topic_base}/BuildTemperature/Name/Sensor2":
            self.temp_2.set(f'Sensor2 temperature: {data["Temperature"]:.2f}')
        elif message.topic == f"{self.topic_base}/BuildTemperature/Name/Sensor3":
            self.temp_3.set(f'Sensor3 temperature: {data["Temperature"]:.2f}')
        elif message.topic == f"{self.topic_base}/BuildTemperature/Name/Sensor4":
            self.temp_4.set(f'Sensor4 temperature: {data["Temperature"]:.2f}')


class PumpsFrame(ttk.LabelFrame):
    def __init__(self, master, grpc_client, *args, **kwargs):
        title = self.__class__.__name__.replace("Frame", "")
        super().__init__(master, *args, text=title, **kwargs)
        self.client = grpc_client

        self.pump = StringVar(value="backing")
        self.create_widgets()
        self.create_grid()

    def command(self, enable):
        try:
            self._command(enable)
        except grpc.RpcError as error:
            print(error.code().name, error.details())

    def _command(self, enable):
        if self.pump.get() == "backing":
            cmd = msg.VacuumGeneratorCommand(active=enable)
            self.client.LowVacuumGenerationControl(cmd)
        else:
            cmd = msg.VacuumGeneratorCommand(
                generator_name=[self.pump.get()], active=enable
            )
            self.client.HighVacuumGenerationControl(cmd)

    def create_widgets(self):
        valid_pumps = ["backing", "build_turbo", "powder_turbo", "gun_turbo"]
        self.cb_pumps = ttk.Combobox(
            self, textvariable=self.pump, values=valid_pumps, state="readonly"
        )
        self.bt_on = ttk.Button(self, text="ON", command=lambda: self.command(True))
        self.bt_off = ttk.Button(self, text="OFF", command=lambda: self.command(False))

    def create_grid(self):
        self.cb_pumps.grid(column=0, row=0, padx=5, pady=5)
        self.bt_on.grid(column=1, row=0, padx=5, pady=5)
        self.bt_off.grid(column=2, row=0, padx=5, pady=5)


class VentingFrame(ttk.LabelFrame):
    def __init__(self, master, grpc_client, *args, **kwargs):
        title = self.__class__.__name__.replace("Frame", "")
        super().__init__(master, *args, text=title, **kwargs)
        self.client = grpc_client

        self.valve = StringVar(value="build_valve")
        self.create_widgets()
        self.create_grid()

    def command(self, enable):
        try:
            self._command(enable)
        except grpc.RpcError as error:
            print(error.code().name, error.details())

    def _command(self, enable):
        cmd = msg.VentingCommand(source=[self.valve.get()], ventilate=enable)
        self.client.VentingControl(cmd)

    def create_widgets(self):
        valid_valves = ["build_valve", "powder_valve", "gun_valve"]
        self.cb_valves = ttk.Combobox(
            self, textvariable=self.valve, values=valid_valves, state="readonly"
        )
        self.bt_open = ttk.Button(self, text="OPEN", command=lambda: self.command(True))
        self.bt_close = ttk.Button(
            self, text="CLOSE", command=lambda: self.command(False)
        )

    def create_grid(self):
        self.cb_valves.grid(column=0, row=0, padx=5, pady=5)
        self.bt_open.grid(column=1, row=0, padx=5, pady=5)
        self.bt_close.grid(column=2, row=0, padx=5, pady=5)


def main(args):
    # Load service configuration
    cfg = Configurator.load_config("chamberservice.yaml")

    rpc_host = cfg.get_str("Service:GRPC:IP")
    rpc_port = cfg.get_int("Service:GRPC:Port")
    channel = grpc.insecure_channel(f"{rpc_host}:{rpc_port}")
    grpc_client = rpc.ChamberServiceStub(channel)

    mqtt_client = mqtt.Client(
        client_id=f"{__package__}_gui", clean_session=True, userdata=(cfg,)
    )
    mqtt_client.connect(host=cfg["Service:MQTT:IP"], port=cfg["Service:MQTT:Port"])
    mqtt_client.loop_start()

    root = Tk()
    root.option_add("*tearOff", FALSE)
    root.title(f"{__package__} client")
    root.columnconfigure(0, weight=1)
    root.rowconfigure(0, weight=1)

    fr_content = ttk.Frame(root, padding=10)
    fr_mqtt = MqttFrame(
        fr_content, mqtt_client, topic_base=cfg["Service:MQTT:TopicBase"]
    )
    fr_pumps = PumpsFrame(fr_content, grpc_client)
    fr_valves = VentingFrame(fr_content, grpc_client)

    _common = dict(sticky="NEWS", padx=5, pady=5)
    fr_content.grid(column=0, row=0, sticky="NSEW", padx=10, pady=10)
    fr_pumps.grid(column=0, row=0, **_common)
    fr_valves.grid(column=0, row=1, **_common)
    fr_mqtt.grid(column=1, row=0, rowspan=20, **_common)

    try:
        root.mainloop()
    finally:
        mqtt_client.disconnect()
        mqtt_client.loop_stop()
        print("bye")
