freemelt-chamberservice (3.0.4-1) stable; urgency=medium

  * No functional changes. Released for technical reasons.

 -- Freemelt AB <opensource@freemelt.com>  Mon, 03 Oct 2022 10:46:17 +0200

freemelt-chamberservice (3.0.3-1) stable; urgency=medium

  * MQTT is now configured before OPC UA.

 -- Daniel Andersson <daniel.andersson@qrtech.se>  Thu, 09 Jun 2022 15:26:14 +0200

freemelt-chamberservice (3.0.2-1) stable; urgency=medium

  * Remove reference to Fore pump (now Backing Pump)

 -- Freemelt AB <opensource@freemelt.com>  Wed, 2 Sep 2020 12:40:33 +0000

freemelt-chamberservice (3.0.1-1) stable; urgency=medium

  * Update default turbopump frequency where we can start ventilation, to 1150 Hz

 -- Freemelt AB <opensource@freemelt.com>  Wed, May 20 11:33:02 2020 +0200

freemelt-chamberservice (3.0.0-1) stable; urgency=medium

  * New upstream release.
  * Breaking change: Package has been renamed to freemelt-chamberservice.
    The old package 'freemelt-vtservice' is now only avaiable as a transition package.
  * Added systemd dependency Wants and After on mosquitto service.

 -- Freemelt AB <opensource@freemelt.com>  Wed, 11 Mar 2020 13:14:47 +0100

freemelt-vtservice (2.0.0-1) stable; urgency=medium

  * New upstream release.
  * Latest proto files from freemeltapi are used. This is a beaking change.
  * Logging made journal aware
  * Changed default HighVacuumPumpStart_PressureMax from 0.1 to 10 mbar

 -- Freemelt AB <opensource@freemelt.com>  Wed, 04 Mar 2020 14:16:46 +0100

freemelt-vtservice (1.3.0-1) stable; urgency=medium

  * Loglevel can be set on specific logger
  * Replaced dependency freemelt-configurator with freemelt-servicelib
  * gRPC methods VacuumLampCommand and CoilSectionFanCommand added
  * Improved error messages

 -- Freemelt AB <opensource@freemelt.com>  Thu, 23 Jan 2020 12:13:58 +0100

freemelt-vtservice (1.2.1-1) stable; urgency=medium

  * Set 0 Celcius when sensor is not available
  * Don’t add the user site-packages directory to sys.path
  * Bugfix in backing pump check

 -- Freemelt AB <opensource@freemelt.com>  Thu, 19 Dec 2019 10:10:44 +0100

freemelt-vtservice (1.2.0-1) stable; urgency=medium

  * Use opcualib.SyncClient instead of opcualib.OPCWrapper.
  * Improved error handling and service stop.

 -- Freemelt AB <opensource@freemelt.com>  Fri, 22 Nov 2019 13:56:11 +0200

freemelt-vtservice (1.1.1-1) stable; urgency=medium

  * The first stable relase. (No changes since prev. tag)

 -- Freemelt AB <opensource@freemelt.com>  Fri, 18 Oct 2019 16:56:11 +0200

freemelt-vtservice (1.1.0-1) unstable; urgency=medium

  * Drops InfluxDB support. Use Freemelt MetricsRelay instead.

 -- Freemelt AB <opensource@freemelt.com>  Wed, 16 Oct 2019 13:45:20 +0200

freemelt-vtservice (1.0.0-1) unstable; urgency=medium

  * Initial release.

 -- Freemelt AB <opensource@freemelt.com>  Thu, 12 Sep 2019 15:53:23 +0200
