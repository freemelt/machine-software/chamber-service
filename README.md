<!--
SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>

SPDX-License-Identifier: GPL-3.0-only
-->

# Chamber Service

Description: `TODO`

Dependencies:
- python3
- opcualib
- grpcio-tools [pip3]

Genereating protobuf messages:
```
python3 -m grpc_tools.protoc -I=./protos --python_out=../src --grpc_python_out=../src ChamberServiceRPC.proto
```

Manually copy the resulting files into `./src`: `ChamberServiceRpc_pb2.py` & `ChamberServiceRpc_pb2_grpc.py`

## Metrics

The `Backingpumps` measurement is only updated when gRPC calls are made. The other measurements are updated continuously by the metrics logger thread.

| Measurement        | Fields                                         | Tags                                              |
| ------------------ | ---------------------------------------------- | ------------------------------------------------- |
| `Backingpumps`     | `On` (boolean)                                 | `Position` (gGun \| Build \| Powder \| Back)      |
| `BuildTemperature` | `Temperature` (float)                          | `Name` (Sensor1 \| Sensor2 \| Sensor3 \| Sensor4) |
| `Turbopumps`       | `Current`, `Frequency`, `Temperature` (floats) | `Position` (gGun \| Build \| Powder)              |
| `Vacuum`           | `Pressure` (float)                             | `Position` (gGun \| Build \| Powder \| Back)      |




# License
Copyright © 2019 - 2021 Freemelt AB <opensource@freemelt.com>

Chamber service is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.